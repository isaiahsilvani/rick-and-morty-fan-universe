package com.example.rickandmorty.di

import com.example.authentication.viewmodel.ForgotPasswordViewModel
import com.example.authentication.viewmodel.LoginViewModel
import com.example.authentication.viewmodel.SignUpViewModel
import com.example.data.repositories.FirebaseRepoImpl
import com.example.domain.repositories.AuthRepo
import com.example.domain.usecases.auth.*
import com.example.domain.usecases.validation.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module() {
    // Firebase
    single { FirebaseAuth.getInstance() }
    single { Firebase.firestore }
    // Auth Repository
    single<AuthRepo> { FirebaseRepoImpl(get(), get()) }
    // Use Cases
    single { CheckEmailUseCase(get()) }
    single { CheckPasswordUseCase(get()) }
    single { CheckUsernameUseCase(get()) }
    single { CheckConfirmPasswordUseCase(get()) }
    single { LoginUseCase(get()) }
    single { CreateAccountUseCase(get())}
    single { CheckUsersUseCase(get()) }
    single { SendResetLinkUseCase(get()) }
    // ViewModels
    viewModel {
        ForgotPasswordViewModel(get())
    }
    viewModel {
        SignUpViewModel(get(), get(), get(), get(), get())
    }
    viewModel {
        LoginViewModel(get(), get())
    }
}