package com.example.explore_feature.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.explore_feature.databinding.ExploreFragmentBinding

class ExploreFragment : Fragment() {

    lateinit var binding: ExploreFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ExploreFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

}