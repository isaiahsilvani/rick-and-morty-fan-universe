package com.example.authentication.view.login

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.authentication.databinding.LoginFragmentBinding
import com.example.authentication.viewmodel.LoginViewModel
import com.example.authentication.viewmodel.SignUpViewModel
import com.example.rickandmorty.view.utils.collectLatestLifecycleFlow
import com.example.rickandmorty.view.utils.navigateTo
import com.example.rickandmorty.view.utils.validateData
import com.google.firebase.auth.FirebaseAuth
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    private lateinit var binding: LoginFragmentBinding
    private val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.checkUserLoggedIn()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LoginFragmentBinding.inflate(layoutInflater)
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        collectLatestLifecycleFlow(viewModel.loginState) { state ->
            val email = binding.etEnterEmail.text.toString()
            val password = binding.etEnterPassword.text.toString()
            if (state.successfulLogin == true) {
                navigateTo(LoginFragmentDirections.actionLoginFragmentToExploreGraph())
            }
            if (state.isValidEmailFormat == false) binding.etEnterEmail.error =
                "Invalid email format"
            binding.progressCircle.isVisible = state.isLoading
            if (state.successfulLogin == false && email.isNotBlank() && password.isNotBlank()) {
                Toast.makeText(requireContext(), "Password and Email Invalid", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            btnSignIn.setOnClickListener {
                viewModel.attemptLogin(etEnterEmail.text.toString(), etEnterPassword.text.toString())
            }
            tvForgetPassword.setOnClickListener {
                navigateTo(LoginFragmentDirections.actionLoginFragmentToForgotPasswordFragment())
            }
            tvCreateAccount.setOnClickListener {
                navigateTo(LoginFragmentDirections.actionLoginFragmentToSignupFragment())
            }
            etEnterEmail.setOnFocusChangeListener { v, _ ->
                if (!v.hasFocus()) viewModel.checkEmailFormat(etEnterEmail.text.toString())
            }
        }
    }
}
