package com.example.authentication.view.signup

data class SignUpState(
    var usernameIsAvailable: Boolean? = null,
    var emailIsAvailable: Boolean? = null,
    var passwordIsValid: Boolean? = null,
    var confirmPasswordIsValid: Boolean? = null,
    var isLoading: Boolean = false,
    var signupisSuccessful: Boolean? = null
)