package com.example.authentication.view.forgotpassword

data class ForgotPasswordState(
    val resetLinkSent: Boolean? = null,
    val isLoading: Boolean = false
)