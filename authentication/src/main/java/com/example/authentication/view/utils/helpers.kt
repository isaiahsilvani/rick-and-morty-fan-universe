package com.example.rickandmorty.view.utils

import android.util.Log
import android.util.Patterns
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest

fun Fragment.navigateTo(action: NavDirections) {
    findNavController().navigate(action)
}

fun <T> Fragment.collectLatestLifecycleFlow(flow: Flow<T>, collect: suspend (T) -> Unit) =
    lifecycleScope.launchWhenStarted {
        flow.collectLatest(collect)
    }

fun Fragment.validateData(
    email: MaterialAutoCompleteTextView,
    password: MaterialAutoCompleteTextView,
    confirmPassword: MaterialAutoCompleteTextView? = null,
    username: MaterialAutoCompleteTextView? = null,
) : Boolean {
    // Means we're in context of a login screen
    val isValidEmailFormat = Patterns.EMAIL_ADDRESS.matcher(email.text).matches()
    val passwordsMatch = (confirmPassword?.text.toString() == password.text.toString())
    Log.e("VALIDATE", "password: ${password.text} | confirmPassword: ${confirmPassword?.text.toString()}")
    val validPassword = password.length() >= 8
    if (confirmPassword == null && username == null) {
        if (!isValidEmailFormat) email.error = "Provide valid email"
        if (password.text.isBlank()) password.error = "Fill out Password"
        return (isValidEmailFormat && password.text.isNotBlank())
    } else if (confirmPassword != null && username != null) {
        Log.e("VALIDATE", "ConfirmPassword and Username is not null")
        // Means we're in context of a signup screen
        if (!isValidEmailFormat) email.error = "Provide valid email"
        if (password.text.isBlank()) password.error = "Fill out Password"
        if (username.text.isBlank()) username.error = "Fill out Username"
        if (confirmPassword.text.isBlank()) confirmPassword.error = "Fill out Confirm Password"
        Log.e("VALIDATE", "Passwords match: $passwordsMatch")
        val test = isValidEmailFormat && passwordsMatch && validPassword && username.text.length > 5
        Log.e("VALIDATE", "The test passes: $test")
        return test
    }
    return false
}

