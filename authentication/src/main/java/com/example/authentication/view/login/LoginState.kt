package com.example.authentication.view.login

data class LoginState(
    val isValidEmailFormat: Boolean? = null,
    val successfulLogin: Boolean? = null,
    val isLoading: Boolean = false
)
