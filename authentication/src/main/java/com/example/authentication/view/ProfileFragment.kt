package com.example.rickandmorty.view

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.authentication.databinding.ProfileFragmentBinding
import com.google.firebase.auth.FirebaseAuth

class ProfileFragment : Fragment() {

    lateinit var binding: ProfileFragmentBinding
    lateinit var firebaseAuth: FirebaseAuth
    lateinit var user: Any

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ProfileFragmentBinding.inflate(layoutInflater)
        firebaseAuth = FirebaseAuth.getInstance()
        binding.textView.text = firebaseAuth.currentUser?.uid
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.button.setOnClickListener {
            firebaseAuth.signOut()
            val url = Uri.parse("android-app://com.example.rickandmorty/login_fragment")
            findNavController().navigate(url)
        }
    }
}