package com.example.authentication.view.forgotpassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.authentication.databinding.ForgotPasswordFragmentBinding
import com.example.authentication.viewmodel.ForgotPasswordViewModel
import com.example.rickandmorty.view.utils.collectLatestLifecycleFlow
import com.example.rickandmorty.view.utils.navigateTo
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class ForgotPasswordFragment : Fragment() {

    lateinit var binding: ForgotPasswordFragmentBinding
    private val viewModel by viewModel<ForgotPasswordViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ForgotPasswordFragmentBinding.inflate(layoutInflater)
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        collectLatestLifecycleFlow(viewModel.forgotPasswordState) { state ->
            when (state.resetLinkSent) {
                true -> Toast.makeText(requireContext(), "RESET EMAIL SENT", Toast.LENGTH_LONG).show()
                false -> Toast.makeText(requireContext(), "EMAIL NOT FOUND", Toast.LENGTH_LONG).show()
                else -> false
            }
            binding.progressCircle.isVisible = state.isLoading
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btnSubmit.setOnClickListener {
            viewModel.sendResetLink(binding.etEmail.text.toString())
        }
        binding.tvGoBack.setOnClickListener {
            navigateTo(ForgotPasswordFragmentDirections.actionForgotPasswordFragmentToLoginFragment())
        }
    }
}