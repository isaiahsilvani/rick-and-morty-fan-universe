package com.example.authentication.view.signup

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.findNavController
import com.example.authentication.databinding.SignupFragmentBinding
import com.example.authentication.viewmodel.SignUpViewModel
import com.example.rickandmorty.view.utils.collectLatestLifecycleFlow
import com.example.rickandmorty.view.utils.navigateTo
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignupFragment : Fragment() {

    private lateinit var binding: SignupFragmentBinding
    private val viewModel: SignUpViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SignupFragmentBinding.inflate(layoutInflater)
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        viewModel.signupState.observe(viewLifecycleOwner) { state ->
            with(binding) {
                Log.e("STATE", "STATE CHANGED OMG")
                progressCircle.isVisible = state.isLoading
                if (state.emailIsAvailable == false) etEnterEmail.error = "Email is not available"
                if (state.usernameIsAvailable == false) etEnterUsername.error = "Username is not available"
                if (state.passwordIsValid == false) etEnterPassword.error = "Password is invalid"
                if (state.confirmPasswordIsValid == false) etConfirmPassword.error = "Passwords do not match"
                if (state.signupisSuccessful == true) {
                    navigateTo(SignupFragmentDirections.actionSignupFragmentToExploreGraph())
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            btnSignUp.setOnClickListener {
                viewModel.createAccount(
                    etEnterEmail.text.toString(),
                    etConfirmPassword.text.toString(),
                    etEnterUsername.text.toString()
                )
            }
            tvCreateAccount.setOnClickListener {
                // Go back to login
                val uri = Uri.parse("android-app://com.example.rickandmorty/login_fragment")
                findNavController().navigate(uri)
            }
            tvForgetPassword.setOnClickListener {
                // Go to forgot password
                val uri = Uri.parse("android-app://com.example.rickandmorty/forgot_password_fragment")
                findNavController().navigate(uri)
            }
            etEnterEmail.setOnFocusChangeListener { v, _ ->
                if (!v.hasFocus()) viewModel.checkEmail(etEnterEmail.text.toString())
            }
            etEnterUsername.setOnFocusChangeListener { v, _ ->
                if (!v.hasFocus()) viewModel.checkUsername(etEnterUsername.text.toString())
            }
            etEnterPassword.setOnFocusChangeListener { v, _ ->
                if (!v.hasFocus() && etEnterPassword.text.isBlank()) {
                    etEnterPassword.error = "Password is blank"
                } else if (!v.hasFocus()){
                    viewModel.checkPassword(etEnterPassword.text.toString())
                }
            }
            etConfirmPassword.setOnFocusChangeListener { v, _ ->
                if (!v.hasFocus()) viewModel.checkConfirmPassword(
                    etEnterPassword.text.toString(), etConfirmPassword.text.toString()
                )
            }
        }
    }

}