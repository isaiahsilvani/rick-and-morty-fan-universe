package com.example.authentication.viewmodel

import android.util.Log
import android.util.Patterns
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.authentication.view.login.LoginState
import com.example.domain.usecases.validation.CheckUsersUseCase
import com.example.domain.usecases.auth.LoginUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class LoginViewModel(
    private val checkUsersUseCase: CheckUsersUseCase,
    private val loginUseCase: LoginUseCase
) : ViewModel() {

    private var _loginState = MutableStateFlow(LoginState())
    val loginState = _loginState.asStateFlow()

    fun checkEmailFormat(email: String) {
        Log.e("CheckEmail", "CheckEmail launched! $email")
        viewModelScope.launch {
            _loginState.value = _loginState.value.copy(
                isValidEmailFormat = Patterns.EMAIL_ADDRESS.matcher(email).matches()
            )
        }
    }

    fun checkUserLoggedIn() {
        viewModelScope.launch {
            checkUsersUseCase().collectLatest { userIsLoggedIn ->
                _loginState.value = _loginState.value.copy(
                    successfulLogin = userIsLoggedIn.getOrNull()
                )
            }
        }
    }

    fun attemptLogin(email: String, password: String) {
        viewModelScope.launch {
            _loginState.value = _loginState.value.copy(isLoading = true)
            val isValidFormat = Patterns.EMAIL_ADDRESS.matcher(email).matches()
            if (email.isNotBlank() && password.isNotBlank() && isValidFormat) {
                loginUseCase(email, password).collectLatest { successfulLogin ->
                    _loginState.value = _loginState.value.copy(
                        successfulLogin = successfulLogin.getOrNull(),
                        isLoading = false
                    )
                }
            } else {
                _loginState.value = _loginState.value.copy(
                    successfulLogin = false,
                    isLoading = false
                )
            }
        }
    }
}