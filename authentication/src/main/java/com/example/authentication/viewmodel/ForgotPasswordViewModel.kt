package com.example.authentication.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.authentication.view.forgotpassword.ForgotPasswordState
import com.example.domain.usecases.auth.SendResetLinkUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ForgotPasswordViewModel(
    private val sendResetLinkUseCase: SendResetLinkUseCase
) : ViewModel() {

    private val _forgotPasswordState = MutableStateFlow(ForgotPasswordState())
    val forgotPasswordState = _forgotPasswordState.asStateFlow()

    fun sendResetLink(email: String) {
        viewModelScope.launch {
            with(_forgotPasswordState) {
                value = ForgotPasswordState(isLoading = true)
                sendResetLinkUseCase(email).collectLatest { linkSent ->
                    value = value.copy(resetLinkSent = linkSent.getOrNull(), isLoading = false)
                }
            }
        }
    }

}