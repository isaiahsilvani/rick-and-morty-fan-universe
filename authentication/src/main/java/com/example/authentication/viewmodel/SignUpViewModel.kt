package com.example.authentication.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.authentication.view.signup.SignUpState
import com.example.domain.usecases.auth.*
import com.example.domain.usecases.validation.CheckConfirmPasswordUseCase
import com.example.domain.usecases.validation.CheckEmailUseCase
import com.example.domain.usecases.validation.CheckPasswordUseCase
import com.example.domain.usecases.validation.CheckUsernameUseCase
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class SignUpViewModel(
    private val checkEmailUseCase: CheckEmailUseCase,
    private val checkUsernameUseCase: CheckUsernameUseCase,
    private val checkPasswordUseCase: CheckPasswordUseCase,
    private val checkConfirmPasswordUseCase: CheckConfirmPasswordUseCase,
    private val createAccountUseCase: CreateAccountUseCase,
) : ViewModel() {

    private var _signupState = MutableLiveData(SignUpState())
    val signupState get() = _signupState

    fun checkEmail(email: String) {
        Log.e("CheckEmail", "CheckEmail launched! $email")
        viewModelScope.launch {
            checkEmailUseCase(email).collectLatest { emailAvailable ->
                Log.e("CheckEmail", "Is email already here? $emailAvailable")
                _signupState.value = _signupState.value?.copy(emailIsAvailable = emailAvailable.getOrNull())
            }
        }
    }

    fun checkPassword(password: String) {
        viewModelScope.launch {
            checkPasswordUseCase(password).collectLatest { isValidPassword ->
                _signupState.value = _signupState.value?.copy(passwordIsValid = isValidPassword.getOrNull())
            }
        }
    }

    fun checkConfirmPassword(password: String, confirmPassword: String) {
        viewModelScope.launch {
            checkConfirmPasswordUseCase(password, confirmPassword).collectLatest { passwordsMatch ->
                _signupState.value = _signupState.value?.copy(confirmPasswordIsValid = passwordsMatch.getOrNull())
            }
        }
    }

    fun createAccount(email: String, password: String, username: String) {
        viewModelScope.launch {
            _signupState.value = _signupState.value?.copy(isLoading = true)
            with(_signupState) {
                if (value?.usernameIsAvailable == true
                    && value?.emailIsAvailable == true
                    && value?.confirmPasswordIsValid == true) {
                    Log.e("TAG", "WE CREATE AN ACCOUNT MAN")
                    createAccountUseCase(email, password, username).collectLatest { signupSuccess ->
                        value = value?.copy(signupisSuccessful = signupSuccess.getOrNull())
                    }
                }
            }
        }
    }

    fun checkUsername(username: String) {
        viewModelScope.launch {
            checkUsernameUseCase(username).collectLatest { isUsernameAvailable ->
                Log.e("TAG", "Is username available? ${isUsernameAvailable.getOrNull()}")
                _signupState.value = _signupState.value?.copy(usernameIsAvailable = isUsernameAvailable.getOrNull())
                Log.e("TAG", _signupState.value.toString() )
            }
        }
    }
}