package com.example.data.repositories

import android.util.Log
import com.example.domain.repositories.AuthRepo
import com.example.domain.usecases.validation.TAG
import com.example.rickandmorty.model.UserModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class FirebaseRepoImpl(
    private val auth: FirebaseAuth,
    private val cloudDB: FirebaseFirestore,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) : AuthRepo {

    override suspend fun checkEmail(email: String) = callbackFlow {
        cloudDB.collection("users")
            .whereEqualTo("email", email)
            .get()
            .addOnSuccessListener {
                Log.e("checkEmail", "There is an email already: ${it.documents.isEmpty()}")
                scope.launch { send(it.documents.isEmpty()) }
            }
            .addOnFailureListener { e ->
                Log.e("ERROR", "There was an error: ${e.message}")
            }
        awaitClose { this.cancel() }
    }

    override suspend fun checkUsername(username: String) = callbackFlow<Boolean> {
        cloudDB.collection("users")
            .whereEqualTo("username", username)
            .get()
            .addOnSuccessListener {
                Log.e("FOUND", "Username is available?: ${it.documents}")
                scope.launch { send(it.documents.isEmpty()) }
            }
            .addOnFailureListener { e ->
                Log.e("ERROR", "There was an error: ${e.message}")
            }
        awaitClose { this.cancel() }
    }

    override suspend fun checkPassword(password: String) = callbackFlow {
        send(password.length > 5)
        awaitClose { this.cancel() }
    }

    override suspend fun checkConfirmPassword(password: String, confirmPassword: String) =
        callbackFlow {
            send(password == confirmPassword)
            awaitClose { this.cancel() }
        }

    override suspend fun checkUserLoggedIn() = callbackFlow {
        val firebaseUser = auth.currentUser
        send(firebaseUser != null)
    }

    override suspend fun sendPasswordResetLink(email: String): Flow<Boolean> = callbackFlow {
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener { task ->
                scope.launch { send(task.isSuccessful) }
            }
        awaitClose { this.cancel() }
    }

    override suspend fun resetPassword(password: String, confirmPassword: String): Flow<Boolean> {
        TODO("Not yet implemented")
    }

    override suspend fun createAccount(email: String, password: String, username: String) =
        callbackFlow {
            Log.e(
                "createAccountRepo",
                "Create an account with email: $email, password: $password, $username"
            )
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.e("createAccountRepo", "SUCCESS!!!!. Store User in firestore")
                        // add user to firestore
                        scope.launch {
                            val user = UserModel(
                                email = email,
                                username = username,
                                uid = auth.uid.toString()
                            )
                            cloudDB.collection("users")
                                .add(user)
                                .addOnSuccessListener { documentReference ->
                                    Log.d(
                                        "SIGNUP",
                                        "DocumentSnapshot added with ID: ${documentReference.id}"
                                    )
                                }
                                .addOnFailureListener { e ->
                                    Log.w("SIGNUP", "Error adding document", e)
                                }
                        }
                        scope.launch { send(true) }
                    } else {
                        Log.e("TASK", "It wasn't successful: ${task.exception?.message}")
                        scope.launch { send(false) }
                    }
                }
            awaitClose { this.cancel() }
        }

    override suspend fun loginAccount(email: String, password: String) = callbackFlow {
        auth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                Log.e(TAG, "Successful login!")
                scope.launch { send(true) }
            }
            .addOnFailureListener { e ->
                Log.e(TAG, "Failed login! :(")
                scope.launch { send(false) }
            }

        awaitClose { this.cancel() }
    }
}