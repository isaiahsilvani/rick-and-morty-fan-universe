package com.example.rickandmorty.model

data class ShallowLocation(
    val name: String,
    val url: String
)