package com.example.rickandmorty.model

data class Favorites(
    val locations: List<Location>,
    val episodes: List<Episode>,
    val characters: List<Character>
)