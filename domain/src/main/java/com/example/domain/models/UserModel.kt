package com.example.rickandmorty.model

data class UserModel(
    val email: String,
    val favorites: Favorites? = null,
    val username: String,
    val uid: String
)