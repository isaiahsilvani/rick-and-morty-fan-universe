package com.example.domain.usecases.auth

import android.util.Log
import com.example.domain.repositories.AuthRepo
import com.example.domain.usecases.validation.TAG
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class LoginUseCase(
    private val authRepo: AuthRepo,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) {
    suspend operator fun invoke(email: String, password: String) = callbackFlow<Result<Boolean>> {
        val job = scope.launch {
            authRepo.loginAccount(email, password)
                .catch { e ->
                    Log.e(TAG, "invoke: There was an error ${e.message}")
                    send(Result.failure(e))
                }
                .collectLatest { loginSuccessful ->
                    send(Result.success(loginSuccessful))
                }
        }
        awaitClose { job.cancel() }
    }
}