package com.example.domain.usecases.validation

import android.util.Log
import com.example.domain.repositories.AuthRepo
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest

class CheckUsernameUseCase(
    private val authRepo: AuthRepo,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) {
    suspend operator fun invoke(username: String) = callbackFlow<Result<Boolean>> {
        Log.e("CHECK", "CHECK USERNAME INVOKE: $username")
        val job = scope.launch {
            authRepo.checkUsername(username)
                .catch { e ->
                    Log.e(TAG, "invoke: There was an error ${e.message}")
                    send(Result.failure(e))
                }
                .collectLatest { usernameAvailable ->
                    Log.e("CHECK", "Is username available?: $usernameAvailable")
                    send(Result.success(usernameAvailable))
                }
        }
        awaitClose {
            job.cancel()
            this.cancel()
        }
    }
}