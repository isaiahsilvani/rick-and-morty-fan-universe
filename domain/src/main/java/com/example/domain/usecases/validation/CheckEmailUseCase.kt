package com.example.domain.usecases.validation

import android.util.Log
import com.example.domain.repositories.AuthRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

const val TAG = "CheckEmailUC"

class CheckEmailUseCase(
    private val authRepo: AuthRepo,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) {
    suspend operator fun invoke(email: String) = callbackFlow<Result<Boolean>> {
        val job = scope.launch {
            authRepo.checkEmail(email)
                .catch { e ->
                    Log.e(TAG, "invoke: There was an error ${e.message}")
                    send(Result.failure(e))
                }
                .collectLatest { emailIsAvailable ->
                    send(Result.success(emailIsAvailable))
                }
        }
        awaitClose { job.cancel() }
    }
}