package com.example.domain.usecases.validation

import android.util.Log
import com.example.domain.repositories.AuthRepo
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest

class CheckUsersUseCase(
    private val authRepo: AuthRepo,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) {
    suspend operator fun invoke() = callbackFlow<Result<Boolean>> {
        val job = scope.launch {
            authRepo.checkUserLoggedIn()
                .catch { e ->
                    Log.e(TAG, "invoke: There was an error ${e.message}")
                    send(Result.failure(e))
                }
                .collectLatest { userIsLoggedIn ->
                    Log.e("CHECK", "Is username available?: $userIsLoggedIn")
                    send(Result.success(userIsLoggedIn))
                }
        }
        awaitClose {
            job.cancel()
            this.cancel()
        }
    }
}