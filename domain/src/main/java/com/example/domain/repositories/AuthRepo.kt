package com.example.domain.repositories

import kotlinx.coroutines.flow.Flow

interface AuthRepo {

    suspend fun createAccount(email: String, password: String, username: String): Flow<Boolean>

    suspend fun loginAccount(email: String, password: String): Flow<Boolean>

    suspend fun checkEmail(email: String): Flow<Boolean>

    suspend fun checkUsername(username: String): Flow<Boolean>

    suspend fun checkPassword(password: String): Flow<Boolean>

    suspend fun checkConfirmPassword(password: String, confirmPassword: String): Flow<Boolean>

    suspend fun checkUserLoggedIn(): Flow<Boolean>

    suspend fun sendPasswordResetLink(email: String): Flow<Boolean>

    suspend fun resetPassword(password: String, confirmPassword: String): Flow<Boolean>

}